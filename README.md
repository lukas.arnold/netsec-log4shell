# Log4Shell example environment

## Prerequisites

A Linux-based machine with
- [Docker Engine](https://docs.docker.com/engine/install/#supported-platforms)
- [Docker Compose V2](https://docs.docker.com/compose/install/compose-plugin/#install-using-the-repository)

## Execution
Simulate the exploit with Docker Compose.
The commands start the vulnerable service, the payload delivery service and the LDAP service.
After 5 seconds, the exploit is executed by sending a message to the vulnerable service.
```bash
docker compose build
docker compose up
```

To manually execute the exploit, open a new terminal and run
```bash
cd exploit
docker build -t log4shell-exploit . && docker run --rm --network host log4shell-exploit
```
After its execution you should see in the log of the vulnerable service, a message printed by an externally loaded class.

## Components

A simple Log4Shell attack happens in the following order:  
```
exploit -> vulnerable -> ldap -> vulnerable -> payload -> vulnerable
```

### exploit

A client for the vulnerable server which sends a malicious message referencing to the LDAP server.

### ldap

The LDAP server which is contacted upon the arrival of a malicious message by the vulnerable service.
It answers the request with an instruction to download the payload.

### payload

The payload of the attacker which is first compiled to a JAR and then served over HTTP.

### vulnerable

A vulnerable application which accepts messages over the TCP port 23 and logs them using Log4J.