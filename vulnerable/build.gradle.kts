plugins {
    id("java")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "de.seemoo.netsec"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

// https://gist.github.com/domnikl/c19c7385927a7bef7217aa036a71d807
tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "de.seemoo.netsec.Log4ShellServer"
    }
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    // Use a vulnerable version of Log4J
    // https://en.wikipedia.org/wiki/Log4Shell#Mitigation
    // https://jfrog.com/blog/log4shell-0-day-vulnerability-all-you-need-to-know/
    implementation("org.apache.logging.log4j:log4j-api:2.14.1")
    implementation("org.apache.logging.log4j:log4j-core:2.14.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}