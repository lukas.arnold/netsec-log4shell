package de.seemoo.netsec;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.*;

public class Log4ShellServer {

    public static void main(String[] args) {
        // Log all message with the level DEBUG or higher - https://stackoverflow.com/a/23434603/4106848
        Configurator.setRootLevel(Level.DEBUG);
        // Construct the logger
        Logger logger = LogManager.getLogger(Log4ShellServer.class);

        // Port for this simple server
        int port = 23;

        // Signal that the server is ready
        logger.info("Server up and running at port {}", port);

        // Create a new ServerSocket to accept connections
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            // Infinite loop to continuously accept new connections (one at a time)
            while (true) {
                // Accepts a new connection
                Socket socket = serverSocket.accept();
                // Create a BufferedReader for reading from the connection
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // Get the source address of the connection
                String address = socket.getInetAddress().getHostAddress();

                // Print that a client has established a new session
                logger.info("Session with {} opened", address);

                // Continuously read messages from the client and log them
                String message;
                while ((message = in.readLine()) != null) {
                    logger.info("Message from {}: {}", address, message);
                }

                // Print that the client has terminated the session
                logger.info("Session with {} terminated", address);
            }
        } catch (IOException ex) {
            logger.warn("IOException cauzsed by the ServerSocket", ex);
        }
    }

}
