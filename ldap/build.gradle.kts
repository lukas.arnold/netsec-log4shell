plugins {
    id("java")
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "de.seemoo.netsec"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

// https://gist.github.com/domnikl/c19c7385927a7bef7217aa036a71d807
tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "de.seemoo.netsec.LDAPServer"
    }
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    // The LDAP SDK comes with a simple modifiable LDAP server
    implementation("com.unboundid:unboundid-ldapsdk:6.0.5")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}